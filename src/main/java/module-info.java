module es.palacios.g {
    requires javafx.controls;
    requires javafx.fxml;

    opens es.palacios.g to javafx.fxml;
    exports es.palacios.g;
}